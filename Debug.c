/*****************************************************************************
 * Module name : Debug
 * Project     : GWS
 *
 * Copyright (C) 2010 Cal-Sierra Communications.
 * All rights reserved.
 *
 * The information contained herein is confidential
 * property of Company. The use, copying, transfer or
 * disclosure of such information is prohibited except
 * by express written agreement with Company.
 *
 * First written on 1/18/2010 by Steve deRosier
 *
 * Module Description:
 *   Debuging functions
 *
 *****************************************************************************/
/*****************************************************************************
 *  Include section
 * Add all #includes here
 *
 *****************************************************************************/
#include ".\Include\Compiler.h"

#if defined(DEBUG)
#include ".\Include\UART.h"

#include ".\Include\Debug.h"

/*****************************************************************************
 *  Defines section
 * Add all #defines here
 *
 *****************************************************************************/

/*****************************************************************************
 *  Function Prototype Section
 * Add prototypes for all functions called by this
 * module, with the exception of runtime routines,
 * or prototypes already defined in included headers.
 *****************************************************************************/

/*****************************************************************************
 * Function name: void DebugPrintStr( char * String )
 *    returns: nothing
 *    String: String to print
 * Created by: SDd
 * Date created: 10/18/2010
 * Description: Print a ram string.
 *****************************************************************************/
void DebugPrintStr( char * String )
{
	putsUART( String );
}

/*****************************************************************************
 * Function name: void DebugPrintRStr( const rom char * String )
 *    returns: nothing
 *    String: String to print
 * Created by: SDd
 * Date created: 10/18/2010
 * Description: Print a constant rom string.
 *****************************************************************************/
void DebugPrintRStr( const char * String )
{
	putrsUART( String );
}

void DebugPrintBytes( int Length, void * Bytes )
{
	int i;
	BOOL FirstTime = TRUE;

	for( i = 0; i < Length; i++ )
	{
		if( (i % 16 == 0) &&
		        !FirstTime )
		{
			DebugPrintRStr("\r\n");
			DebugPrintHex(i);
			DebugPrintRStr("   ");
		}

		DebugPrintHex( *((UINT8 *)Bytes + i) );
		DebugPrintRStr(" ");
		FirstTime = FALSE;
	}

}

#endif

