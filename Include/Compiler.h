/*********************************************************************
 *
 *                  Compiler and hardware specific definitions
 *
 *********************************************************************
 * FileName:        Compiler.h
 * Dependencies:    None
 * Processor:       PIC18, PIC24F, PIC24H, dsPIC30F, dsPIC33F
 ********************************************************************/
#ifndef COMPILER_H
#define COMPILER_H

/* Do board specific defines */
#ifdef _18F2620
#define __18CXX
#include <xc.h>

#include "GenericTypeDefs.h"

#include "brd_18F2620.h"

#elif defined(_10F322)
#define __10CXX
#include <xc.h>

#include "GenericTypeDefs.h"

#include "brd_10F322.h"

#endif

/* Everything here below should be generic stuff OK for both boards */
#if defined(DEBUG)
#define DebugPrint(a)       {putrsUART(a);}
#else
#define DebugPrint(a)       {}
#endif

#define ON	1
#define OFF	0

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#ifdef __C30__
#include <stdlib.h>
#endif

#define getcUART()              ReadUART()
#define putcUART(a)             WriteUART(a)


#endif

