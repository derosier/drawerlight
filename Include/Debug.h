/*****************************************************************************
 * Module name : Debug
 * Project     : GWS
 * Interface (header) File
 *
 * Copyright (C) 2010 Cal-Sierra Communications
 * All rights reserved.
 *
 * The information contained herein is confidential
 * property of Company. The use, copying, transfer or
 * disclosure of such information is prohibited except
 * by express written agreement with Company.
 *
 * First written on 1/18/2010 by Steve deRosier
 *
 * Module Description:
 *    Contains debug functions and definitions
 *
 *****************************************************************************/

/* Change MODULENAME_H_ to match your module name.
 * This ensures that this header doesn't get
 * included multiple times accidently              */
#ifndef DEBUG_H_
#define DEBUG_H_

/* Don't change the following 3 lines.
 * This checks and throws an error incase you
 * didn't customize the above define.              */
#ifdef MODULENAME_H_
#error Module name define not set in header 
#endif

#include "Compiler.h"

/* Everything that you put in this file goes below
 * this line.  It is importaint to keep everything
 * within the #ifndef MODULENAME_H_ .. #endif pair */
#ifdef DEBUG
/*****************************************************************************
 *  Include section
 * Add all #includes here
 *
 *****************************************************************************/
#include "UART.h"

/*****************************************************************************
 *  Defines section
 * Add all #defines here
 *
 *****************************************************************************/

/*****************************************************************************
 *  Function Prototype Section
 * Add prototypes for all functions called by this
 * module, with the exception of runtime routines.
 *
 *****************************************************************************/
void DebugPrintStr( char * String );
void DebugPrintRStr( const char * String );
void DebugPrintBytes( int Length, void * Bytes );

/*****************************************************************************
 * MACRO name: DebugPrintVal( Value )
 *    returns: nothing
 *    Value: Value to print
 * Created by: SDd
 * Date created: 10/18/2010
 * Description: Output a value in base 10.
 *****************************************************************************/
#define DebugPrintChar( Value ) {\
    while(BusyUART()); \
    putcUART(Value); \
}

#define DebugPrintVal( Value )  {\
   char dbgText[12];\
   itoa( dbgText, Value, 10 );\
   putsUART( dbgText );\
   }

#define DebugPrintHex( Value )  {\
   char dbgText[12];\
   itoa( dbgText, Value, 16 );\
   putsUART( dbgText );\
   }


#else

// These go away if debug is not defined, so they compile to nothing
#define DebugPrintStr( S ) {}
#define DebugPrintRStr( S ) {}
#define DebugPrintBytes( L, V ) {}

#define DebugPrintChar( V ) {}
#define DebugPrintVal( V ) {}
#define DebugPrintHex( V ) {}

#endif
/* Don't touch this last line.  Nothing should be
 * entered below here.                             */
#endif


