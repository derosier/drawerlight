/*****************************************************************************
 * Module name : Sensor
 * Project     : GWS
 * Interface (header) File
 *
 * Copyright (C) 2010 Cal-Sierra Communications
 * All rights reserved.
 *
 * The information contained herein is confidential
 * property of Company. The use, copying, transfer or
 * disclosure of such information is prohibited except
 * by express written agreement with Company.
 *
 * First written on 1/18/2010 by Steve deRosier
 *
 * Module Description:
 *   This module handles all setup, reading and other parts of handling
 *  the mosture sensor.
 *
 *****************************************************************************/

/* Change MODULENAME_H_ to match your module name.
 * This ensures that this header doesn't get
 * included multiple times accidently              */
#ifndef SENSOR_H_
#define SENSOR_H_

/* Don't change the following 3 lines.
 * This checks and throws an error incase you
 * didn't customize the above define.              */
#ifdef MODULENAME_H_
#error Module name define not set in header 
#endif

/* Everything that you put in this file goes below
 * this line.  It is importaint to keep everything
 * within the #ifndef MODULENAME_H_ .. #endif pair */

/*****************************************************************************
 *  Include section
 * Add all #includes here
 *
 *****************************************************************************/
#include "GenericTypeDefs.h"

/*****************************************************************************
 *  Defines section
 * Add all #defines here
 *
 *****************************************************************************/

/*****************************************************************************
 *  Function Prototype Section
 * Add prototypes for all functions called by this
 * module, with the exception of runtime routines.
 *
 *****************************************************************************/
void Sensor_InitHW( void );
void Sensor_TurnOnSensor( void );
void Sensor_TurnOffSensor( void );
BYTE Sensor_ReadSensor( void );

/* Don't touch this last line.  Nothing should be
 * entered below here.                             */
#endif


