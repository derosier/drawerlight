/*********************************************************************
 *
 *                  Tick Manager for PIC18
 *
 *********************************************************************
 * FileName:        Tick.h
 * Dependencies:    None
 * Processor:       PIC18, PIC24F, PIC24H, dsPIC30F, dsPIC33F
 * Complier:        Microchip C18 v3.02 or higher
 *                  Microchip C30 v2.01 or higher
 * Company:         Microchip Technology, Inc.
 *
 * Software License Agreement
 *
 * This software is owned by Microchip Technology Inc. ("Microchip")
 * and is supplied to you for use exclusively as described in the
 * associated software agreement.  This software is protected by
 * software and other intellectual property laws.  Any use in
 * violation of the software license may subject the user to criminal
 * sanctions as well as civil liability.  Copyright 2006 Microchip
 * Technology Inc.  All rights reserved.
 *
 * This software is provided "AS IS."  MICROCHIP DISCLAIMS ALL
 * WARRANTIES, EXPRESS, IMPLIED, STATUTORY OR OTHERWISE, NOT LIMITED
 * TO MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, AND
 * INFRINGEMENT.  Microchip shall in no event be liable for special,
 * incidental, or consequential damages.
 *
 *
 * Author               Date    Comment
 *~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Nilesh Rajbharti     6/28/01 Original        (Rev 1.0)
 * Nilesh Rajbharti     2/9/02  Cleanup
 * Nilesh Rajbharti     5/22/02 Rev 2.0 (See version.log for detail)
 ********************************************************************/

#ifndef TICK_H
#define TICK_H

#include "Compiler.h"

typedef UINT16 TICK;

#if !defined(TICKS_PER_SECOND)
#error TICKS_PER_SECOND must be defined.
#endif

#if !defined(TICK_PRESCALE_VALUE)
#error TICK_PRESCALE_VALUE must be defined.
#endif

#if (TICK_PRESCALE_VALUE == 2)
#define TIMER_PRESCALE  (0)
#elif ( TICK_PRESCALE_VALUE == 4 )
#define TIMER_PRESCALE  (1)
#elif ( TICK_PRESCALE_VALUE == 8 )
#define TIMER_PRESCALE  (2)
#elif ( TICK_PRESCALE_VALUE == 16 )
#define TIMER_PRESCALE  (3)
#elif ( TICK_PRESCALE_VALUE == 32 )
#define TIMER_PRESCALE  (4)
#elif ( TICK_PRESCALE_VALUE == 64 )
#define TIMER_PRESCALE  (5)
#elif ( TICK_PRESCALE_VALUE == 128 )
#define TIMER_PRESCALE  (6)
#elif ( TICK_PRESCALE_VALUE == 256 )
#define TIMER_PRESCALE  (7)
#else
#error Invalid TICK_PRESCALE_VALUE specified.
#endif

#define TICK_TEMP_VALUE_1       \
		((INSTR_FREQ) / (TICKS_PER_SECOND * TICK_PRESCALE_VALUE))

#if defined(TIMER_16BIT)
	#if TICK_TEMP_VALUE_1 > 60000
	#error TICK_PER_SECOND value cannot be programmed with current CLOCK_FREQ
	#error Either lower TICK_PER_SECOND or manually configure the Timer
	#endif
	
	#define TICK_TEMP_VALUE         (65535 - TICK_TEMP_VALUE_1)
	
	#define TICK_COUNTER_HIGH       ((TICK_TEMP_VALUE >> 8) & 0xff)
	#define TICK_COUNTER_LOW        (TICK_TEMP_VALUE & 0xff)
	
#elif defined(TIMER_8BIT)
	#if TICK_TEMP_VALUE_1 > 250
	#error TICK_PER_SECOND value cannot be programmed with current CLOCK_FREQ
	#error Either lower TICK_PER_SECOND or manually configure the Timer
	#endif
	
	#define TICK_TEMP_VALUE         ((255 - TICK_TEMP_VALUE_1) + 2)
	
	#define TICK_COUNTER        	(TICK_TEMP_VALUE & 0xff)
#else
	#error Not a supported timer
#endif

#define TICK_PERIOD_MS          10
#define TICK_SECOND             ((TICK)TICKS_PER_SECOND)
#define MS_TICKS(a)             ((a)/TICK_PERIOD_MS)

//#define TickGetDiff(a, b)       (a-b)
#define TickGetDiff(a, b)       (TICK)(a < b) ? (((TICK)0xffff - b) + a) : (a - b)

/*
 * Only Tick.c defines TICK_INCLUDE and thus defines Seconds
 * and TickValue storage.
 */
#ifndef TICK_INCLUDE
extern volatile TICK TickCount;
#endif

/*********************************************************************
 * Function:        void TickInit(void)
 *
 * PreCondition:    None
 *
 * Input:           None
 *
 * Output:          Tick manager is initialized.
 *
 * Side Effects:    None
 *
 * Overview:        Initializes Timer0 as a tick counter.
 *
 * Note:            None
 ********************************************************************/
void TickInit(void);



/*********************************************************************
 * Function:        TICK TickGet(void)
 *
 * PreCondition:    None
 *
 * Input:           None
 *
 * Output:          Current second value is given
 *
 * Side Effects:    None
 *
 * Overview:        None
 *
 * Note:            None
 ********************************************************************/
TICK TickGet(void);

/*********************************************************************
 * Function:        bool TickTick(void)
 *
 * PreCondition:    None
 *
 * Input:           None
 *
 * Output:          True if a tick has happened since last call.
 *
 * Side Effects:    reset TickFlag
 *
 * Overview:        None
 *
 * Note:            None
 ********************************************************************/
BOOL TickTick( BOOL * TickMiss );

/*********************************************************************
 * Function:        bool TickTick(void)
 *
 * PreCondition:    None
 *
 * Input:           None
 *
 * Output:          True if a tick has happened since last call.
 *
 * Side Effects:    reset TickFlag
 *
 * Overview:        None
 *
 * Note:            None
 ********************************************************************/
BOOL Yet(UINT32 Next);

/*********************************************************************
 * Function:        bool TickMiss(void)
 *
 * PreCondition:    None
 *
 * Input:           None
 *
 * Output:          True more than one tick has happened since last call to TickTick.
 *
 * Side Effects:    reset TickFlag and TickFlagMiss
 *
 * Overview:        None
 *
 * Note:            None
 ********************************************************************/
/* bool TickMiss(void);*/

#endif
