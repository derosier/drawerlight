#ifndef BRD_10F322_H
#define BRD_10F322_H

// Clock frequency value.
// This value is used to calculate Tick Counter value

// Must undefine debug for our code, otherwise we don't fit.
// We use this ID to control many things, mostly inclusion of UART and debug code
#undef DEBUG

#define CLOCK_FREQ          (4000000)      // Hz
#define INSTR_FREQ          (CLOCK_FREQ/4)

#define TICKS_PER_SECOND               (100)        // 10ms

#if (TICKS_PER_SECOND < 10 || TICKS_PER_SECOND > 255)
#error Invalid TICKS_PER_SECONDS specified.
#endif

/*
 * Manually select prescale value to achieve necessary tick period
 * for a given clock frequency.
 */
#define TICK_PRESCALE_VALUE             (64)

#if (TICK_PRESCALE_VALUE != 2 && \
        TICK_PRESCALE_VALUE != 4 && \
        TICK_PRESCALE_VALUE != 8 && \
        TICK_PRESCALE_VALUE != 16 && \
        TICK_PRESCALE_VALUE != 32 && \
        TICK_PRESCALE_VALUE != 64 && \
        TICK_PRESCALE_VALUE != 128 && \
        TICK_PRESCALE_VALUE != 256 )
#error Invalid TICK_PRESCALE_VALUE specified.
#endif

#define TIMER_8BIT

/////////////////////////////////////////////////////////////////
// I/O pins
/////////////////////////////////////////////////////////////////

// LED1 and LED2 are both on the same pin w/ a FET on the Rev B board. So we can
// stay main-code compatible, define both
#define LED_TRIS               (TRISA1)
#define LED_IO                 (RA1)
#define LED                    (LATA1)

#define IR_TRIS               	(TRISA2)
#define IR_IO                 	(RA2)
#define IR                    	(LATA2)

// A/D Pins
#define IR_SENSOR_TRIS     		(TRISA0)
#define IR_SENSOR          		(RA0)

#define IR_AD_CHANNEL      		0
#define AD_MAX_CHANNELS         1

#define xCHS		ADCONbits.CHS
#define xADCS		ADCONbits.ADCS

/////////////////////////////////////////////////////////////////
// Function prototypes
/////////////////////////////////////////////////////////////////

// #define Do_Sleep() \
// {\
// 	SWDTEN = 1;\
// 	SLEEP();\
// 	SWDTEN = 0;\
// }
void Do_Sleep(void);

// Utility
#define Heartbeat()
#define PrintIntro()
#define OutputSensor(SVal)



#endif