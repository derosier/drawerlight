#ifndef BRD_18F2620_H
#define BRD_18F2620_H

// Clock frequency value.
// This value is used to calculate Tick Counter value

#define CLOCK_FREQ          (8000000)      // Hz
#define INSTR_FREQ          (CLOCK_FREQ/4)

#define TICKS_PER_SECOND               (100)        // 10ms

#if (TICKS_PER_SECOND < 10 || TICKS_PER_SECOND > 255)
#error Invalid TICKS_PER_SECONDS specified.
#endif

/*
 * Manually select prescale value to achieve necessary tick period
 * for a given clock frequency.
 */
#define TICK_PRESCALE_VALUE             (8)

#if (TICK_PRESCALE_VALUE != 2 && \
        TICK_PRESCALE_VALUE != 4 && \
        TICK_PRESCALE_VALUE != 8 && \
        TICK_PRESCALE_VALUE != 16 && \
        TICK_PRESCALE_VALUE != 32 && \
        TICK_PRESCALE_VALUE != 64 && \
        TICK_PRESCALE_VALUE != 128 && \
        TICK_PRESCALE_VALUE != 256 )
#error Invalid TICK_PRESCALE_VALUE specified.
#endif

#define TIMER_16BIT
#define USE_UART

/////////////////////////////////////////////////////////////////
// Serial port calculations
/////////////////////////////////////////////////////////////////
#define BAUD_RATE       (57600)     // bps

// The SPBRG_VAL is used for PIC18s only.
#if defined(USART_USE_BRGH_LOW)
#define SPBRG_VAL   ( ((INSTR_FREQ/BAUD_RATE)/16) - 1)
#else
#define SPBRG_VAL   (INSTR_FREQ/BAUD_RATE)
#endif

#if (SPBRG_VAL > 255) && !defined(__C30__)
#error "Calculated SPBRG value is out of range for currnet CLOCK_FREQ."
#endif

/////////////////////////////////////////////////////////////////
// I/O pins
/////////////////////////////////////////////////////////////////

// LED_RED is on RB3
// LED_GREEN is on RB4
#define LED1_TRIS               (TRISB0)
#define LED1_IO                 (RB0)
#define LED1                    (LATB0)
#define LED2_TRIS               (TRISB1)
#define LED2_IO                 (RB1)
#define LED2                    (LATB1)
#define LED						LED1 = LED2

#define IR_TRIS               	(TRISC2)
#define IR_IO                 	(RC2)
#define IR                    	(LATC2)

#define DBG0_TRIS               (TRISC3)
#define DBG0_IO                 (RC3)
#define DBG0                    (LATC3)
#define DBG1_TRIS               (TRISC0)
#define DBG1_IO                 (RC0)
#define DBG1                    (LATC0)


// A/D Pins
#define IR_SENSOR_TRIS     		(TRISA0)
#define IR_SENSOR          		(RA0)

#define IR_AD_CHANNEL      		0
#define AD_MAX_CHANNELS         1

#define xCHS		ADCON0bits.CHS
#define xADCS		ADCON2bits.ADCS
#define xACQT		ADCON2bits.ACQT
#define xADFM		ADFM
#define xPCFG		ADCON1
/////////////////////////////////////////////////////////////////
// Function prototypes
/////////////////////////////////////////////////////////////////

void Do_Sleep(void);

// Utility
void Heartbeat(void);
void PrintIntro( void );
void OutputSensor(unsigned int SVal);



#endif