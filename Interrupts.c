/*********************************************************************
********************************************************************/
#include "Tick.h"

void __interrupt TickUpdate(void)
{
	// Timer 0 is tick
	if( INTCONbits.TMR0IF )
	{
#if defined(TIMER_16BIT)
		TMR0H = TICK_COUNTER_HIGH;
		TMR0L = TICK_COUNTER_LOW;
#elif defined(TIMER_8BIT)
	TMR0 = TICK_COUNTER;
#endif
		TickCount++;

		INTCONbits.TMR0IF = 0;
	}
}

