
all: DrawerLight.X pic10

pic18:
	$(MAKE) --directory='DrawerLight.X' CONF=18F2620

pic10:
	$(MAKE) --directory='DrawerLight.X' CONF=10F322

clean:
	$(MAKE) --directory='DrawerLight.X' clean
	-rm -f *.p1 *.pre
	-rm -f *.obj *.sdb
	-rm -f funclist
	-rm -f *.hex *.cof *.hxl
	-rm -f *.lst *.map *.rlf *.sym
