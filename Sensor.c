/*****************************************************************************
 * Module name : Sensor
 * Project     : GWS
 *
 * Copyright (C) 2010 Cal-Sierra Communications.
 * All rights reserved.
 *
 * The information contained herein is confidential
 * property of Company. The use, copying, transfer or
 * disclosure of such information is prohibited except
 * by express written agreement with Company.
 *
 * First written on 1/18/2010 by Steve deRosier
 *
 * Module Description:
 *   This module handles all setup, reading and other parts of handling
 *  the mosture sensor.
 *
 *****************************************************************************/

/*****************************************************************************
 *  Include section
 * Add all #includes here
 *
 *****************************************************************************/
#include "Compiler.h"
#include "Debug.h"
#include "Delay.h"

/*****************************************************************************
 *  Defines section
 * Add all #defines here
 *
 *****************************************************************************/
#ifdef _18F2620
#define SENSOR_AD_ACQT  0b100  // 8 Tad acquisition time
#define SENSOR_AD_CLK   0b101  // Per table 19-1, 16 Tosc
#else
#define SENSOR_AD_ACQT  0b100  // 8 Tad acquisition time
#define SENSOR_AD_CLK   0b101  // Per table 19-1, 16 Tosc
#endif
/*****************************************************************************
 *  Function Prototype Section
 * Add prototypes for all functions called by this
 * module, with the exception of runtime routines,
 * or prototypes already defined in included headers.
 *****************************************************************************/
BYTE Sensor_ReadSensor( void )
{
#ifdef _18F2620
	// Start conversion
	xCHS = 0x0F & IR_AD_CHANNEL;
	xACQT = 0x07 & SENSOR_AD_ACQT;
	xADCS = 0x07 & SENSOR_AD_CLK;
	xADFM = 0x01 & AD_FORMAT_RIGHT;

	// Configure ADCON2
	xADCS = 0x07 & SENSOR_AD_CLK;
	ADON = 1;
	GO_nDONE = 1;  // start conversion


	// wait
	while( GO_nDONE )
	{
		NOP();
	}

	ADON = 0;


	return (ADRES >> 2);
#else
	// Start conversion
	xCHS = 0x0F & IR_AD_CHANNEL;

	// Configure ADCON2
	xADCS = 0x07 & SENSOR_AD_CLK;
	ADON = 1;
	GO_nDONE = 1;  // start conversion


	// wait
	while( GO_nDONE )
	{
		NOP();
	}

	ADON = 0;

	return ADRES;
#endif
}

