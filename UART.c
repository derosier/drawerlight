/*********************************************************************
 *
 *     UART access routines for C18 and C30
 *
 *********************************************************************
 * FileName:        UART.c
 * Dependencies:    UART.h
 * Processor:       PIC18, PIC24F/H, dsPIC30F, dsPIC33F
 * Complier:        Microchip C18 v3.03 or higher
 * Complier:        Microchip C30 v2.01 or higher
 * Company:         Microchip Technology, Inc.
 *
 * Software License Agreement
 *
 * This software is owned by Microchip Technology Inc. ("Microchip")
 * and is supplied to you for use exclusively as described in the
 * associated software agreement.  This software is protected by
 * software and other intellectual property laws.  Any use in
 * violation of the software license may subject the user to criminal
 * sanctions as well as civil liability.  Copyright 2006 Microchip
 * Technology Inc.  All rights reserved.
 *
 * This software is provided "AS IS."  MICROCHIP DISCLAIMS ALL
 * WARRANTIES, EXPRESS, IMPLIED, STATUTORY OR OTHERWISE, NOT LIMITED
 * TO MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, AND
 * INFRINGEMENT.  Microchip shall in no event be liable for special,
 * incidental, or consequential damages.
 *
 *
 * Author               Date        Comment
 *~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Howard Schlunder     4/04/06     Copied from dsPIC30 libraries
 * Howard Schlunder     6/16/06     Added PIC18
********************************************************************/
#define THIS_IS_UART

#include ".\Include\Compiler.h"
#include ".\Include\UART.h"

#if defined(USE_UART)

BYTE ReadStringUART(BYTE * Dest, BYTE BufferLen)
{
	BYTE c;
	BYTE count = 0;

	while(BufferLen--)
	{
		*Dest = '\0';

		while(!DataRdyUART());
		c = ReadUART();

		if(c == '\r' || c == '\n')
		{
			break;
		}

		count++;
		*Dest++ = c;
	}

	return count;
}


//
// PIC18
//


char BusyUART(void)
{
	return !TXSTAbits.TRMT;
}

void CloseUART(void)
{
	RCSTA &= 0b01001111;  // Disable the receiver
	TXSTAbits.TXEN = 0;   // and transmitter

	PIE1 &= 0b11001111;   // Disable both interrupts
}

char DataRdyUART(void)
{
	if(RCSTAbits.OERR)
	{
		RCSTAbits.CREN = 0;
		RCSTAbits.CREN = 1;
	}
	return PIR1bits.RCIF;
}

char ReadUART(void)
{
	return RCREG;                     // Return the received data
}

void WriteUART(char data)
{
	TXREG = data;      // Write the data byte to the USART
}

void getsUART(char * buffer, unsigned char len)
{
	char i;    // Length counter
	unsigned char data;

	for(i = 0; i < len; i++) // Only retrieve len characters
	{
		while(!DataRdyUART());// Wait for data to be received

		data = getcUART();    // Get a character from the USART
		// and save in the string
		*buffer = data;
		buffer++;              // Increment the string pointer
	}
}

void putsUART( char * data)
{
	while(*data)
	{
		// Transmit a byte
		while(BusyUART());
		putcUART(*data);
		data++;
	}
}

void putrsUART(const char * data)
{
	while(*data)
	{
		// Transmit a byte
		while(BusyUART());
		putcUART(*data);
		data++;
	}
}

#endif  //STACK_USE_UART
