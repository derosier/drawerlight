#ifdef _10F322

#include "Compiler.h"
#include "Tick.h"

void Do_Sleep(void)
{
	// This uses the watchdog to recover from sleep, so have to enable the
	// WD right before sleeping.
	SWDTEN = 1;
	SLEEP();
	SWDTEN = 0;
}

#define PERIOD (INSTR_FREQ/TICK_PRESCALE_VALUE/1000)*TICK_PERIOD_MS
void TickInit(void)
{
	// Setup the timer.
	T0CS = 0;
	PSA = 0;
	OPTION_REGbits.PS = TIMER_PRESCALE;
	
	// Set the timer for our period
	TMR0 = TICK_COUNTER;
	
	// Reset the interrupts
	TMR0IF = 1;
	TMR0IE = 1;

}

#endif