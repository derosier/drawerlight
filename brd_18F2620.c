#ifdef _18F2620

#include "Compiler.h"
#include "Tick.h"
#include "Version.h"
#define DEBUG
#include "Debug.h"


const char CRLF[] = "\r\n";
const char StartText[] = "\r\n\r\n\r\n"
                       "DrawerLight\r\n"
                       "Version: "_VERSION", "__DATE__"\r\n"
                       _COPYRIGHT"\r\n\r\n";
const char SensorLabel[] = "Sensor Value:\t";


void Do_Sleep(void)
{
	// This uses the watchdog to recover from sleep, so have to enable the
	// WD right before sleeping.
	SWDTEN = 1;
	SLEEP();
	SWDTEN = 0;
}



void Heartbeat(void)
{
	DBG0 ^= 1;
	return;
}

void PrintIntro( void )
{
	putrsUSART( StartText );
	return;
}

void OutputSensor(unsigned int SVal)
{
	char Text[10];
	memset(Text, 0, sizeof(Text));
	DebugPrintRStr("Min Open ");
	itoa( Text, (int)SVal, 10 );

	putrsUSART( SensorLabel );
	putsUSART( Text );
	putrsUSART( CRLF );
	return;
}

#define PERIOD (INSTR_FREQ/TICK_PRESCALE_VALUE/1000)*TICK_PERIOD_MS
void TickInit(void)
{
	// Start the timer.
	TMR0L = TICK_COUNTER_LOW;
	TMR0H = TICK_COUNTER_HIGH;

	// 16-BIT, internal timer, PSA set to 1:256
	T0CON = 0b00000000 | TIMER_PRESCALE;

	// Start the timer.
	TMR0ON = 1;

	TMR0IF = 1;
	TMR0IE = 1;

}

#endif
