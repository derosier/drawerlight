/******************************************************************************
 *  File:           main.c
 *  Function:
 *  Author(s):      Steve deRosier
 *  Copyright:      Copyright (c) 2012 Steve deRosier
 *                  All Rights Reserved.
 *
 *  Source:         Started anew.
 *
 *  Notes:
 *
 *  Change History:
 *
 *****************************************************************************/

#include "Compiler.h"
#include "Tick.h"
#include "Version.h"
#include "Sensor.h"

#define DEBUG
#include "Debug.h"

//******************************************************************************
//  Configuration Fuses
//******************************************************************************


#ifdef _18F2620
// The 18F2620 on RevA
// Fail-safe clock disabled; osc switchover disabled; Internal CLK, output
__CONFIG(1, FCMDIS & IESODIS & RCIO);
// Brown-out reset @ 2.6, software control; power-up timer disable,
//   WD disable; WD post-scale 1K == ~0.25 seconds
__CONFIG(2, SWBOREN & BORV21 & PWRTDIS & WDTDIS & WDTPS64 );
// MCLR enabled, Port-B is digital on reset
__CONFIG(3, PBDIGITAL & MCLREN );
// Extended Inst disabled; Debug enabled, Single-supply ICSP disabled
//   Stack over/under flow reset enabled
__CONFIG(4, XINSTDIS & DEBUGEN & LVPDIS & STVREN);
// All protections disabled
__CONFIG(5, UNPROTECT);
__CONFIG(6, UNPROTECT);
__CONFIG(7, UNPROTECT);
#elif _10F322
// Production config:
#pragma config CONFIG = 0x0448

// Debug config:
/*
#pragma config FOSC = INTOSC    // Oscillator Selection bits (INTOSC oscillator: CLKIN function disabled)
#pragma config BOREN = ON       // Brown-out Reset Enable (Brown-out Reset enabled)
#pragma config WDTE = ON        // Watchdog Timer Enable (WDT enabled)
#pragma config PWRTE = ON      // Power-up Timer Enable bit (PWRT disabled)
#pragma config MCLRE = ON       // MCLR Pin Function Select bit (MCLR pin function is MCLR)
#pragma config CP = ON         // Code Protection bit (Program memory code protection is disabled)
#pragma config LVP = OFF        // Low-Voltage Programming Enable (High-voltage on MCLR/VPP must be used for programming)
#pragma config LPBOR = ON       // Brown-out Reset Selection bits (BOR enabled)
#pragma config BORV = LO        // Brown-out Reset Voltage Selection (Brown-out Reset Voltage (Vbor), low trip point selected.)
#pragma config WRT = 0x3        // Flash Memory Self-Write Protection (Write protection off)
*/

#else
#error "Incorrect chip setting for a DL-RevA."
#endif


//******************************************************************************
//  Constants and Configuration
//******************************************************************************
#define LEDFLASH_COUNT 10
#define TimeOutLoops  240

//******************************************************************************
//  Main data structures
//******************************************************************************
static TICK LastHeartbeat = 0;
static BYTE HalfSeconds = 0;
static BYTE OpenSensor = 255; // Lowest sensor value for open drawer
static INT16	TimeOutCnt = 0;

typedef enum { RM_INIT = 0, RM_TRANS_OPEN, RM_OPEN, RM_TRANS_DIM, RM_DIM, RM_CLOSED, RM_MAXMODE } RunModes;


//******************************************************************************
//  ISR
//******************************************************************************
// The system tick ISR is in Tick.c.  This was done to avoid having the ISR
// actually make a function call.

//******************************************************************************
//  Functions
//******************************************************************************
RunModes LoopInit(void);
RunModes LoopOpen(BYTE SensorVal);
RunModes LoopDim(BYTE SensorVal);
RunModes LoopClosed(BYTE SensorVal);
RunModes LoopFadeUp(BYTE SensorVal);

BOOL SensorClosed(BYTE SensorVal);
BOOL SensorOpen(BYTE SensorVal);


// Initialization
void InitHW( void );

void main( void )
{
	RunModes RunMode = RM_INIT;
	BYTE SensorVal;

	// Do all setup first
	InitHW();

	TickInit();

	// Do one-time start actions
	PrintIntro();

	// Main loop
	while( 1 )
	{
		IR = ON;
		// Read sensor
		SensorVal = Sensor_ReadSensor();
		IR = OFF;

		switch( RunMode )
		{
			case RM_INIT:
				RunMode = LoopInit();
				break;

			case RM_TRANS_OPEN:
				RunMode = LoopFadeUp(SensorVal);
				TimeOutCnt = TimeOutLoops;
				break;
			case RM_OPEN:
				RunMode = LoopOpen(SensorVal);
				break;

			case RM_TRANS_DIM:
				TimeOutCnt = TimeOutLoops >> 2;
			case RM_DIM:
				RunMode = LoopDim(SensorVal);
				LED = OFF; // when we come out, we're always in closed.
				break;
			case RM_CLOSED:
				RunMode = LoopClosed(SensorVal);
				break;
		}

		// Sleep
		Do_Sleep(); // Roughly 1/4 second sleep
	} // End main loop

	// Note: main doesn't return/end
}

RunModes LoopInit(void)
{
	BOOL Run = TRUE;
	BYTE SensorVal;
	BYTE i = 0;

	LED = ON;
	Do_Sleep();
	Do_Sleep();
	LED = OFF;
	IR = ON;
	Do_Sleep();
	Do_Sleep();
	LED = ON;

	while(Run)
	{
		SensorVal = Sensor_ReadSensor();
		if( SensorVal < OpenSensor )
		{
			OpenSensor = SensorVal;
		}
		i++;

		if( i > 150 )
		{
			Run = FALSE;
		}
	}

	IR = OFF;

	OutputSensor(OpenSensor);

	Do_Sleep();
	LED = OFF;
	Do_Sleep();
	Do_Sleep();
	LED = ON;
	Do_Sleep();
	Do_Sleep();
	LED = OFF;
	Do_Sleep();
	Do_Sleep();

	return RM_TRANS_OPEN;
}

RunModes LoopOpen(BYTE SensorVal)
{
	LED = ON;

	TimeOutCnt--;
	if( TimeOutCnt == 0 )
	{
		return RM_TRANS_DIM;
	}

	if( SensorClosed(SensorVal) )
	{
		LED = OFF;
		return RM_CLOSED;
	}

	return RM_OPEN;
}

RunModes LoopDim(BYTE SensorVal)
{
	PWM2CON = 0; // Clear PWM2CON
	PR2 = 0xFF;// PWM period
	PWM2DCH = 0x4f; // clear PWM2DCH and PWM2DCL<7:6>
	PWM2DCL = 0;
	// Configure and start timer2
	T2CONbits.T2CKPS = 0x1; // configure T2CKPS of T2CON
	T2CONbits.TMR2ON = 1;// enable timer2 via TMR2ON of T2CON
	PWM2CON = 0xE0; // Above both: PWM enabled, output enabled, current value 1


	while(TimeOutCnt >= 0)
	{
		// First do time accounting, decrement TimeOutCnt each 1/4 second
		if( TickGetDiff(TickGet(), LastHeartbeat) >= (TICK_SECOND >> 2) )
		{
			LastHeartbeat = TickGet();
			TimeOutCnt--;
		}
		IR = ON;
		// Read sensor
		SensorVal = Sensor_ReadSensor();
		IR = OFF;

		if( SensorClosed(SensorVal) )
		{
			break;
		}
	}


	// If we get here due to timeout, TimeOutCnt will be -1;
	// If we get here due to SensorClosed(), TimeOutCnt will be >=0;
	// We can use that fact in LoopClosed() to block and require a
	// sensor closed detection before checking open again.
	LED = OFF;
	PWM2CON = 0;
	T2CON = 0;
	return RM_CLOSED;
}

RunModes LoopClosed(BYTE SensorVal)
{
	LED = OFF;

	if( TimeOutCnt < 0 )
	{
		// We "closed" due to a time out. We have to detect a close to clear this
		// exception, then we are allowed to process open.  Otherwise, we simply
		// blink the leds and immediately go to open state.
		if( SensorClosed(SensorVal) )
		{
			TimeOutCnt = 0;
		}
	}
	else
	{
		if( SensorOpen(SensorVal) )
		{
			return RM_TRANS_OPEN;
		}
	}

	return RM_CLOSED;
}

RunModes LoopFadeUp(BYTE SensorVal)
{
	RunModes NextMode = RM_CLOSED;

	//
	// Setup
	//

	// Disable PWMx pin output via TRIS bit
	PWM2CON = 0; // Clear PWM2CON
	PR2 = 0xFF;// PWM period
	PWM2DCH = 0; // clear PWM2DCH and PWM2DCL<7:6>
	PWM2DCL = 0;
	// Configure and start timer2
		//-- clear TMR2IF of PIR1
		T2CONbits.T2CKPS = 0x1; // configure T2CKPS of T2CON
		T2CONbits.TMR2ON = 1;// enable timer2 via TMR2ON of T2CON
	// Configure PWM loading PWM2CON w/ values
	// Enable PWM output and --wait for TMR2IF bit set
	// Enable PWM2 pin out by setting TRIS and PWMxOE
	PWM2CON = 0xC0; // Above both: PWM enabled, output enabled, current value 0


	//
	// Cycle through fade ups
	//
	for( LastHeartbeat = TickGet(); PWM2DCH != 0xFF; )
	{
		// Each fade position is one tick, aprox 10 ms
		if( TickGetDiff(TickGet(), LastHeartbeat) >= (1) )
		{
			LastHeartbeat = TickGet();
			PWM2DCH++;
		}

		IR = ON;
		// Read sensor
		SensorVal = Sensor_ReadSensor();
		IR = OFF;

		if( SensorClosed(SensorVal) )
		{
			LED = OFF;
			goto FU_Done;
		}
	}

	//
	// Return pin to normal GPIO control
	//
	NextMode = RM_OPEN;
	LED = ON;
FU_Done:
	PWM2CON = 0;
	T2CON = 0;
	return NextMode;
}


BOOL SensorClosed(BYTE SensorVal)
{
	if( SensorVal < (OpenSensor - 8))
	{
		return TRUE;
	}

	return FALSE;
}

BOOL SensorOpen(BYTE SensorVal)
{
	if(SensorVal > (OpenSensor - 5))
	{
		return TRUE;
	}

	return FALSE;
}


void InitHW( void )
{
#ifdef _18F2620
	// Setup OSC
	OSCCONbits.IDLEN = 0;     // Device enteres Sleep on SLEEP instruction
	OSCCONbits.IRCF = 0b111;  // 8 MHz
	OSCTUNEbits.INTSRC = 1;   // 31.25 derived from INTOSC
	OSCTUNEbits.PLLEN = 0;    // PLL disabled

	//////////////////////////////////////////////////////////////////////////
	// Port A hardware configuration pins:
	//   RA0 -  A/D In -> IR sensor input
	//   RA6 -  INTOSC /4 output
	// RA0 is analog input, the rest of PORTA should be digital
	ADCON1 = 0x0E; // AN0 only, Vref from VCC and GND.

	// Clear Port A
	// Tristate 0 is output, 1 is input
	//   Setting RA7 to output and high.  This is optimal for our design (pin is
	//    tied to Vcc via R5).
	LATA = 0x00;
	TRISA = 0xFF;

	//////////////////////////////////////////////////////////////////////////
	// Port B hardware configuration pins:
	//   RB0 -  Digital Out -> LED1
	//   RB1 -  Digital Out -> LED2
	//   RB6 -  ISD PGC
	//   RB7 -  ISD PGD

	// Clear Port B
	// Tristate 0 is output, 1 is input
	LATB = 0;
	TRISB = 0xFF; // All are inputs unless otherwise stated.

	LED1 = 0;
	LED2 = 0;
	LED1_TRIS = 0;  // 0 is output
	LED2_TRIS = 0;

	//////////////////////////////////////////////////////////////////////////
	// Port C hardware configuration pins:
	//   RC0 -  Digital Out -> DBG1
	//   RC2 -  Digital Out -> IR_LED
	//   RC3 -  Digital Out -> DBG0
	//   RC6 -  UART -> TX
	//   RC7 -  UART -> RX

	// Clear Port C
	// Tristate 0 is output, 1 is input
	LATC = 0;
	TRISC = 0xFF;

	IR = 0;
	IR_TRIS = 0;

	DBG1 = 0;
	DBG1_TRIS = 0;
	DBG0 = 0;
	DBG0_TRIS = 0;


	//////////////////////////////////////////////////////////////////////////
	// Setup USART
	SPEN = 1;
	TRISC7 = 1;
	TRISC6 = 1;

#ifdef USART_USE_BRGH_LOW
	TXSTA = 0b00100000;     // Low BRG speed
#else
	TXSTA = 0b00100100;     // High BRG speed
	BRG16 = 1;
#endif
	RCSTA = 0b10010000;
	CREN = 0; // We're not ever using Rx anymore
	SPBRG = SPBRG_VAL;

	// Enable Interrupts
	T0CON = 0;
	INTCONbits.GIEH = 1;
	INTCONbits.GIEL = 1;

#elif defined(_10F322)
	// Setup OSC
	CLKRCONbits.CLKROE = 0;   // Disable clk output
	OSCCONbits.IRCF = 0b101;  // 4 MHz

	//////////////////////////////////////////////////////////////////////////
	// Port A hardware configuration pins:
	//   RA0 -  A/D In -> IR sensor input
	//   RA1 - LED
	//   RA2 - IR_LED
	//   RA3 - /MCLR

	// Clear Port A
	// Tristate 0 is output, 1 is input
	//   Setting RA7 to output and high.  This is optimal for our design (pin is
	//    tied to Vcc via R5).
	WPUA = 0; // clear weak pullups
	nWPUEN = 1; // Disable pullups
	LATA = 0x00; // Clear outputs
	TRISA = 0xFF; // First set as inputs

	// RA0 is analog input, the rest of PORTA should be digital
	ANSELA = 0; // Clear first, Vref from VCC and GND.
	ANSA0 = 1;  // Set RA0 as analog in

	LED_TRIS = 0;  // 0 is output
	IR_TRIS = 0;

	// Set Vreg powersave
	VREGPM1 = 1;

	// Set WD
	WDTCONbits.WDTPS = 0b01000; // Set WD at ~0.25 second

	// Enable Interrupts
	INTCONbits.GIE = 1;
#else
#error "Only valid targets are PIC18F2620 and PIC10F322 for this board."
#endif

	return;
}
